FROM golang:1.15.2-alpine3.12 as build-env

ENV GO111MODULE=on  \
    CGO_ENABLED=0   \
    GOOS=linux      \
    GOARCH=amd64

WORKDIR /build
COPY . .
RUN go build -o app cmd/main.go


FROM alpine:3.12

RUN apk update && \
    apk add git

WORKDIR /workspace

WORKDIR /go/bin
COPY --from=build-env /build/app .
RUN chmod +x app
ENTRYPOINT ["/go/bin/app"]
