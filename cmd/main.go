package main

import (
	"fmt"
	"html"
	"net"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lwlee2608-matrixx/golang-helloworld/util"
)

func init() {
	log.SetFormatter(&log.TextFormatter{
		TimestampFormat: "2006-01-02T15:04:05.000",
		FullTimestamp:   true,
		ForceColors:     true,
	})
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})

	port := "8080"
	l, err := net.Listen("tcp", ":"+port)
	if err != nil {
		log.Fatal(err)
	}

	utility := util.New("")
	utility.Foo()

	log.Printf("Server started at port %v", port)

	if err := http.Serve(l, nil); err != nil {
		log.Fatal(err)
	}
}
