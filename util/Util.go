package util

import "time"

type Util struct {
	namespace string
	interval  time.Duration
}

func New(namespace string) *Util {
	return &Util{
		namespace: namespace,
		interval:  3 * time.Second,
	}
}

func (u *Util) Foo() {
}
