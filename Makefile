GO = $(shell which go 2>/dev/null)
DOCKER = $(shell which docker 2>/dev/null)

APP=go-hello

ifeq ($(GO),)
$(warning "go is not in your system PATH")
else
$(info "go found")
endif

ifeq ($(DOCKER),)
$(warning "docker is not in your system PATH")
else
$(info "docker found")
endif


.PHONY: all

all: clean build docker

clean:
	$(RM) $(APP)
build:
	$(GO) build -o $(APP) cmd/main.go
docker:
	$(DOCKER) build . -t golang-helloworld
